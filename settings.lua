data:extend {
	{
		default_value = 12.727922061,
		name = '_7ac00258_087c_44a1_aa38_cdd9348a45c2',
		setting_type = 'startup',
		type = 'double-setting'
	},
	{
		default_value = 6.180339887,
		name = '_d6f02d46_d58b_4899_b30c_d85c1be122bf',
		setting_type = 'startup',
		type = 'double-setting'
	},
	{
		default_value = 9.9,
		name = '_df1205e3_01bc_4772_9f30_7b180294dad9',
		setting_type = 'startup',
		type = 'double-setting'
	},
	{
		default_value = 404.50849725,
		name = '_7b4288c3_2852_4847_a0de_6beb29a132a9',
		setting_type = 'startup',
		type = 'double-setting'
	},
	{
		default_value = 12.944271912,
		name = '_f3e8da50_ad53_455e_a81c_8558c8a9885f',
		setting_type = 'startup',
		type = 'double-setting'
	},
	{
		default_value = 0.45,
		name = '_a4196491_b4e6_4f18_af90_efb9d8931fba',
		setting_type = 'startup',
		type = 'double-setting'
	},
	{
		default_value = 16.180339887,
		name = '_ea4956c6_2a1a_4ce5_8f5a_3ecbe5a0e4ed',
		setting_type = 'startup',
		type = 'double-setting'
	},
	{
		default_value = {r = 0.6, g = 0, b = 1, a = 0},
		name = '_4c0a0965_870e_4a7c_b2bc_28012ebe3576',
		setting_type = 'startup',
		type = 'color-setting'
	},
}
