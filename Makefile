FACTORIO_MOD_NAME ?= $(shell jq -r .name info.json)
FACTORIO_MOD_VER ?= $(shell jq -r .version info.json)
FACTORIO_MOD_ZIP_NAME ?= ${FACTORIO_MOD_NAME}_${FACTORIO_MOD_VER}.zip
FACTORIO_MOD_DIR ?= ${HOME}/.factorio/mods

${FACTORIO_MOD_ZIP_NAME}: \
changelog.txt \
data-final-fixes.lua \
data.lua \
graphics \
icon.png \
info.json \
locale \
migrations \
settings.lua \
thumbnail.png
	mkdir _
	cp -r --reflink=auto $^ _
	#zip -ry9 $@ _
	advzip -4a $@ _
	$(RM) -r _

all: ${FACTORIO_MOD_ZIP_NAME}

${FACTORIO_MOD_DIR}/${FACTORIO_MOD_ZIP_NAME}: ${FACTORIO_MOD_ZIP_NAME}
	cp --reflink=auto $< $@

install: ${FACTORIO_MOD_DIR}/${FACTORIO_MOD_ZIP_NAME}

clean:
	${RM} -r _ ${FACTORIO_MOD_ZIP_NAME}

.PHONY: all clean install
