local MOD_PATH = '__lexxy-wickerbeast__'
local gfx_path = MOD_PATH .. '/graphics/'

-- pawprint code

local pawbprint_name = '_88d2cb7d_cf3f_4a84_9aa8_c7ef21d088cd'

data:extend {
	util.merge {
		data.raw['optimized-particle']['character-footprint-particle'],
		{
			name = pawbprint_name,
			pictures = {
				sheet = {
					filename = gfx_path .. 'footprints.png',
					height = 38,
					scale = 0.25,
					shift = util.by_pixel(0, 1),
					usage = 'player',
					variation_count = 8,
					width = 38
				}
			}
		}
	}
}

-- shared animations

local stripes = function (input)
	local output = {}
	for i, v in ipairs(input) do
		output[i] = {
			filename = gfx_path .. v .. '.png',
			height_in_frames = 1,
			width_in_frames = 22
		}
	end
	return output
end

local icons = {{
	icon = MOD_PATH .. '/icon.png'
}}

local level1_dead = {
	filename = gfx_path .. 'level1_dead.png',
	frame_count = 4,
	height = 264,
	scale = 0.3,
	shift = util.by_pixel(0, 5),
	usage = 'player',
	width = 216
}

local level1_dead_shadow = {
	draw_as_shadow = true,
	filename = gfx_path .. 'level1_dead_shadow.png',
	frame_count = 4,
	height = 132,
	scale = 0.6,
	shift = util.by_pixel(3, 8),
	usage = 'player',
	width = 108
}

local level1_flipped_running_gun_shadow = {
	-- factorio doesn't [yet] support symlinks... u.u
	stripes = stripes {
		'level1_running_gun_shadow-01',
		'level1_running_gun_shadow_flipped-02',
		'level1_running_gun_shadow-03',
		'level1_running_gun_shadow_flipped-04',
		'level1_running_gun_shadow_flipped-05',
		'level1_running_gun_shadow_flipped-06',
		'level1_running_gun_shadow_flipped-07',
		'level1_running_gun_shadow_flipped-08',
		'level1_running_gun_shadow_flipped-09',
		'level1_running_gun_shadow_flipped-10',
		'level1_running_gun_shadow_flipped-11',
		'level1_running_gun_shadow_flipped-12',
		'level1_running_gun_shadow_flipped-13',
		'level1_running_gun_shadow_flipped-14',
		'level1_running_gun_shadow_flipped-15',
		'level1_running_gun_shadow-16',
		'level1_running_gun_shadow-15',
		'level1_running_gun_shadow-18'
	},
	direction_count = 18,
	draw_as_shadow = true,
	frame_count = 22,
	height = 94,
	scale = 0.6,
	shift = util.by_pixel(23, 4),
	usage = 'player',
	width = 162
}

local level1_idle_gun = {
	animation_speed = 0.15,
	direction_count = 8,
	filename = gfx_path .. 'level1_idle_gun.png',
	frame_count = 22,
	height = 264,
	scale = 0.3,
	shift = util.by_pixel(0, -17),
	usage = 'player',
	width = 216
}

local level1_idle_gun_shadow = {
	direction_count = 8,
	draw_as_shadow = true,
	filename = gfx_path .. 'level1_idle_gun_shadow.png',
	frame_count = 22,
	height = 94,
	scale = 0.6,
	shift = util.by_pixel(23, 4),
	usage = 'player',
	width = 162
}

local level1_mining_tool = {
	animation_speed = 0.5,
	direction_count = 8,
	filename = gfx_path .. 'level1_mining_tool.png',
	frame_count = 22,
	height = 320,
	scale = 0.3,
	shift = util.by_pixel(0, -18),
	usage = 'player',
	width = 320
}

local level1_mining_tool_shadow = {
	direction_count = 8,
	draw_as_shadow = true,
	filename = gfx_path .. 'level1_mining_tool_shadow.png',
	frame_count = 22,
	height = 125,
	scale = 0.6,
	shift = util.by_pixel(14, -2),
	usage = 'player',
	width = 198
}

local level1_running = {
	animation_speed = 0.95,
	direction_count = 8,
	filename = gfx_path .. 'level1_running.png',
	frame_count = 22,
	height = 264,
	scale = 0.3,
	shift = util.by_pixel(0, -16),
	usage = 'player',
	width = 216
}

local level1_running_shadow = {
	direction_count = 8,
	draw_as_shadow = true,
	filename = gfx_path .. 'level1_running_shadow.png',
	frame_count = 22,
	height = 94,
	scale = 0.6,
	shift = util.by_pixel(23, 4),
	usage = 'player',
	width = 162
}

local level1_running_gun = {
	stripes = stripes {
		'level1_running_gun-01',
		'level1_running_gun-02',
		'level1_running_gun-03',
		'level1_running_gun-04',
		'level1_running_gun-05',
		'level1_running_gun-06',
		'level1_running_gun-07',
		'level1_running_gun-08',
		'level1_running_gun-09',
		'level1_running_gun-10',
		'level1_running_gun-11',
		'level1_running_gun-12',
		'level1_running_gun-13',
		'level1_running_gun-14',
		'level1_running_gun-15',
		'level1_running_gun-16',
		'level1_running_gun-17',
		'level1_running_gun-18'
	},
	animation_speed = 0.8,
	direction_count = 18,
	frame_count = 22,
	height = 264,
	scale = 0.3,
	shift = util.by_pixel(0, -17.5),
	usage = 'player',
	width = 216
}

local level1_running_gun_shadow = {
	stripes = stripes {
		'level1_running_gun_shadow-01',
		'level1_running_gun_shadow-02',
		'level1_running_gun_shadow-03',
		'level1_running_gun_shadow-04',
		'level1_running_gun_shadow-05',
		'level1_running_gun_shadow-06',
		'level1_running_gun_shadow-07',
		'level1_running_gun_shadow-08',
		'level1_running_gun_shadow-09',
		'level1_running_gun_shadow-10',
		'level1_running_gun_shadow-11',
		'level1_running_gun_shadow-12',
		'level1_running_gun_shadow-13',
		'level1_running_gun_shadow-14',
		'level1_running_gun_shadow-15',
		'level1_running_gun_shadow-16',
		'level1_running_gun_shadow-17',
		'level1_running_gun_shadow-18'
	},
	direction_count = 18,
	draw_as_shadow = true,
	frame_count = 22,
	height = 94,
	scale = 0.6,
	shift = util.by_pixel(23, 4),
	usage = 'player',
	width = 162
}

local level2_dead = {
	filename = gfx_path .. 'level2addon_dead.png',
	frame_count = 4,
	height = 264,
	scale = 0.6,
	shift = util.by_pixel(0, 5),
	usage = 'player',
	width = 216
}

local level2_corpse_picture = {
	layers = {
		level1_dead,
		level2_dead,
		level1_dead_shadow
	}
}

-- character entity definition

_3aecca88_5f97_447e_b7dd_46f834ad3c03 = {
	character = {
		animations = {
			{
				idle = {
					layers = {
						{
							animation_speed = 0.15,
							direction_count = 8,
							filename = gfx_path .. 'level1_idle.png',
							frame_count = 22,
							height = 264,
							scale = 0.3,
							shift = util.by_pixel(0, -17),
							usage = 'player',
							width = 216
						},
						{
							direction_count = 8,
							draw_as_shadow = true,
							filename = gfx_path .. 'level1_idle_shadow.png',
							frame_count = 22,
							height = 94,
							scale = 0.6,
							shift = util.by_pixel(23, 4),
							usage = 'player',
							width = 162
						}
					}
				},
				idle_with_gun = {
					layers = {
						level1_idle_gun,
						level1_idle_gun_shadow
					}
				},
				mining_with_tool = {
					layers = {
						level1_mining_tool,
						level1_mining_tool_shadow
					}
				},
				running_with_gun = {
					layers = {
						level1_running_gun,
						level1_running_gun_shadow
					}
				},
				flipped_shadow_running_with_gun = level1_flipped_running_gun_shadow,
				running = {
					layers = {
						level1_running,
						level1_running_shadow
					}
				}
			},
			{
				armors = {},
				idle = {
					layers = {
						{
							animation_speed = 0.15,
							direction_count = 8,
							filename = gfx_path .. 'level2_idle.png',
							frame_count = 22,
							height = 264,
							scale = 0.3,
							shift = util.by_pixel(0, -17),
							usage = 'player',
							width = 216
						},
						{
							direction_count = 8,
							filename = gfx_path .. 'level2addon_idle.png',
							frame_count = 22,
							height = 264,
							scale = 0.3,
							shift = util.by_pixel(-0.25, -17),
							usage = 'player',
							width = 216
						},
						{
							direction_count = 8,
							draw_as_shadow = true,
							filename = gfx_path .. 'level2_idle_shadow.png',
							frame_count = 22,
							height = 94,
							scale = 0.6,
							shift = util.by_pixel(23, 4),
							usage = 'player',
							width = 162,
						}
					}
				},
				idle_with_gun = {
					layers = {
						level1_idle_gun,
						{
							direction_count = 8,
							filename = gfx_path .. 'level2addon_idle_gun.png',
							frame_count = 22,
							height = 264,
							scale = 0.3,
							shift = util.by_pixel(0, -17.5),
							usage = 'player',
							width = 216
						},
						level1_idle_gun_shadow
					}
				},
				mining_with_tool = {
					layers = {
						level1_mining_tool,
						{
							direction_count = 8,
							filename = gfx_path .. 'level2addon_mining_tool.png',
							frame_count = 22,
							height = 320,
							scale = 0.3,
							shift = util.by_pixel(0, -18),
							usage = 'player',
							width = 320
						},
						level1_mining_tool_shadow
					}
				},
				running_with_gun = {
					layers = {
						level1_running_gun,
						{
							direction_count = 18,
							frame_count = 22,
							height = 264,
							scale = 0.3,
							shift = util.by_pixel(0, -17.5),
							stripes = stripes {
								'level2addon_running_gun-01',
								'level2addon_running_gun-02',
								'level2addon_running_gun-03',
								'level2addon_running_gun-04',
								'level2addon_running_gun-05',
								'level2addon_running_gun-06',
								'level2addon_running_gun-07',
								'level2addon_running_gun-08',
								'level2addon_running_gun-09',
								'level2addon_running_gun-10',
								'level2addon_running_gun-11',
								'level2addon_running_gun-12',
								'level2addon_running_gun-13',
								'level2addon_running_gun-14',
								'level2addon_running_gun-15',
								'level2addon_running_gun-16',
								'level2addon_running_gun-17',
								'level2addon_running_gun-18'
							},
							usage = 'player',
							width = 216
						},
						level1_running_gun_shadow
					}
				},
				flipped_shadow_running_with_gun = level1_flipped_running_gun_shadow,
				running = {
					layers = {
						level1_running,
						{
							direction_count = 8,
							filename = gfx_path .. 'level2addon_running.png',
							frame_count = 22,
							height = 264,
							scale = 0.3,
							shift = util.by_pixel(0, -17),
							usage = 'player',
							width = 216
						},
						level1_running_shadow
					}
				}
			}
		},
		character_corpse = '_f42f38fb_b84e_4b34_8fd8_70cce8873d5b',
		damage_hit_tint = settings.startup._4c0a0965_870e_4a7c_b2bc_28012ebe3576.value,
		distance_per_frame = 0.20,
		fast_replaceable_group = 'character',
		footprint_particles = util.merge {
			data.raw.character.character.footprint_particles,
			{{
				particle_name = pawbprint_name
			}}
		},
		healing_per_tick = settings.startup._7ac00258_087c_44a1_aa38_cdd9348a45c2.value / 60,
		icons = icons,
		left_footprint_frames = { 12 },
		light = {
			color = { r = 0.75, g = 0.5, b = 0.85 },
			intensity = 0.75,
			minimum_darkness = 0.3,
			size = 32
		},
		localised_name = { '_a86a3684_b05b_442b_8354_e41a6e3eec5b' }, -- shown on the green button in minime
		max_health = settings.startup._7b4288c3_2852_4847_a0de_6beb29a132a9.value,
		mining_speed = settings.startup._a4196491_b4e6_4f18_af90_efb9d8931fba.value,
		mining_with_tool_particles_animation_positions = { 7 },
		name = '_d97eeff8_9859_4dd9_b592_536d71362532_skin',
		respawn_time = settings.startup._ea4956c6_2a1a_4ce5_8f5a_3ecbe5a0e4ed.value,
		right_footprint_frames = { 2 },
		running_sound_animation_positions = { 10, 20 },
		running_speed = settings.startup._df1205e3_01bc_4772_9f30_7b180294dad9.value / 60,
		ticks_to_stay_in_combat = settings.startup._d6f02d46_d58b_4899_b30c_d85c1be122bf.value * 60,
		tool_attack_result = {
			type = 'direct',
			action_delivery = {
				type = 'instant',
				target_effects = {
					type = 'damage',
					damage = {
						amount = settings.startup._f3e8da50_ad53_455e_a81c_8558c8a9885f.value,
						type =  'physical'
					}
				}
			}
		}
	},
	corpse = {
		icons = icons,
		localised_name = { '_95a60ac2_2177_44a7_bd1b_f57069ea4ee8' }, -- displayed in map marker after death
		name = '_f42f38fb_b84e_4b34_8fd8_70cce8873d5b',
		pictures = {
			{
				layers = {
					level1_dead,
					level1_dead_shadow
				}
			},
			level2_corpse_picture,
			level2_corpse_picture,
			level2_corpse_picture -- what is this fourth entry? is it really for a corpse with armour??
		}
	}
}

CharModHelper.create_prototypes(_3aecca88_5f97_447e_b7dd_46f834ad3c03)
